import React, {useEffect, useState} from 'react';
import Wrapper from "./Wrapper";
import {Account, PublicKey, Transaction} from "@solana/web3.js";
import {getOwnedTokenAccounts} from "../utils/getOwnedTokenAccounts";
import {Button, Input, Modal} from "antd";
import ButtonGroup from "antd/es/button/button-group";
import {createTransferSolTransaction} from "../utils/createTransferSolTransaction";
import {WalletAdapter} from "../wallet-adapters";
import getAllBalances, {Balances} from "../utils/getAllBalances";
import {createTokenAccount} from "../utils/createTokenAccount";
import {TOKEN_MINTS} from "@project-serum/serum";
import setBlockHash from "../utils/setBlockHash";
import {solanaConnection} from "../utils/getConnection";
import {SEND_OPTIONS} from "../utils/options";
import {waitTransactionConfirmation} from "../utils/waitTransactionConfirmation";
import {notify} from "../utils/notifications";
import transferToken from "../utils/transferToken";
import AccountsTable from "./AccountsTable";

export type Balance = { sol: number, usdc: number, hasUsdc: boolean };
export const getDefaultBalance = (): Balance => ({ sol: 0, usdc: 0, hasUsdc: false });

export const getBalancesFromBC = async (accountPublicKey: PublicKey): Promise<Balance> => {
  const balances = await getAllBalances(accountPublicKey);
  const sol = balances.find(({ coin }) => coin === "SOL");
  const balance: Balance = getDefaultBalance();
  if (sol) {
    balance.sol = sol.walletBalance;
  }
  const usdc = balances.find(({ coin }) => coin === "USDC");
  if (usdc) {
    balance.usdc = usdc.walletBalance;
    balance.hasUsdc = true;
  }

  return balance;
}

export const SEND_SOLS_TO_ALL_ACCOUNTS = "SEND_SOLS_TO_ALL_ACCOUNTS";
export const WITHDRAW_SOLS_FROM_ALL_ACCOUNTS = "WITHDRAW_SOLS_FROM_ALL_ACCOUNTS";

export const SEND_USDC_TO_ALL_ACCOUNTS = "SEND_USDC_TO_ALL_ACCOUNTS";
export const WITHDRAW_USDC_FROM_ALL_ACCOUNTS = "WITHDRAW_USDC_FROM_ALL_ACCOUNTS";
export const ADD_USDC_TOKEN = "ADD_USDC_TOKEN";

const Accounts = ({ account, wallet, signWallet, accounts }: { account: Account, wallet: WalletAdapter, signWallet: (tx: Transaction, signers?: Account[]) => void, accounts: Account[]}) => {
  const [transferSolModalIsVisible, setTransferSolModalIsVisible] = useState(false);
  const [solToTransfer, setSolToTransfer] = useState("");
  const [transferSolLoading, setTransferSolLoading] = useState(false);

  const [withdrawSolModalIsVisible, setWithdrawSolModalIsVisible] = useState(false);
  const [solToWithdraw, setSolToWithdraw] = useState("");
  const [withdrawSolLoading, setWithdrawSolLoading] = useState(false);

  const [transferUSDCModalIsVisible, setTransferUSDCModalIsVisible] = useState(false);
  const [usdcToTransfer, setUSDCToTransfer] = useState("");
  const [transferUSDCLoading, setTransferUSDCLoading] = useState(false);

  const [withdrawUSDCModalIsVisible, setWithdrawUSDCModalIsVisible] = useState(false);
  const [usdcToWithdraw, setUSDCToWithdraw] = useState("");
  const [withdrawUSDCLoading, setWithdrawUSDCLoading] = useState(false);

  const [addUSDCMintLoading, setAddUSDCMintLoading] = useState(false);

  const [walletBalance, setWalletBalance] = useState<Balance>(getDefaultBalance());
  const [accountBalance, setAccountBalance] = useState<Balance>(getDefaultBalance());

  const [loading, setLoading] = useState(false);

  const loadBalances = async () => {
    setLoading(true);
    const walletBalance = await getBalancesFromBC(wallet.publicKey);
    setWalletBalance(walletBalance);
    const accountBalance = await getBalancesFromBC(account.publicKey);
    setAccountBalance(accountBalance);
    setLoading(false);
  }

  useEffect(() => {
    loadBalances();
  }, []);

  const transferSol = async () => {
    setTransferSolLoading(true);
    const transaction = createTransferSolTransaction(+solToTransfer.replace(",", "."), wallet.publicKey, account.publicKey);
    try {
      await signWallet(transaction);
    } catch (e) {
      // do nothing
    }
    setTransferSolModalIsVisible(false);
    setTransferSolLoading(false);
    loadBalances();
  }
  
  const withdrawSol = async () => {
    setWithdrawSolLoading(true);
    const transaction = createTransferSolTransaction(+solToWithdraw.replace(",", "."), account.publicKey, wallet.publicKey);
    try {
      await bridgeAccountSign(transaction);
    } catch (e) {
      // do nothing
    }
    setWithdrawSolModalIsVisible(false);
    setWithdrawSolLoading(false);
  }

  const transferUSDC = async () => {
    setTransferUSDCLoading(true);
    const transaction = await transferToken(wallet.publicKey, account.publicKey, +usdcToTransfer.replace(",", "."));
    if (transaction) {
      await signWallet(transaction);
    }
    setTransferUSDCModalIsVisible(false);
    setTransferUSDCLoading(false);
    loadBalances();
  }
  
  const withdrawUSDC = async () => {
    setWithdrawUSDCLoading(true);
    const transaction = await transferToken(account.publicKey, wallet.publicKey, +usdcToWithdraw.replace(",", "."));
    if (transaction) {
      await bridgeAccountSign(transaction);
    }
    setWithdrawUSDCModalIsVisible(false);
    setWithdrawUSDCLoading(false);
    loadBalances();
  }

  const bridgeAccountSign = async (transaction: Transaction, signers?: Account[]) => {
    await setBlockHash(transaction);
    transaction.feePayer = account.publicKey;

    if (signers && signers.length > 0) {
      transaction.partialSign(...signers);
    }

    let signature;

    try {
      transaction.partialSign(account);
      signature = await solanaConnection.sendRawTransaction(transaction.serialize(), SEND_OPTIONS);
      await waitTransactionConfirmation(signature);
      notify({ message: "Confirmed", txid: signature });
    } catch(e) {
      console.log(e);
      notify({ message: "Error while confirming", txid: signature, type: "error" });
    }
  }

  const addUSDCMint = async () => {
    setAddUSDCMintLoading(true);
    const USDC_MINT = TOKEN_MINTS.find(({ name }) => name === "USDC");
    if (USDC_MINT) {
      const [transaction, signers] = await createTokenAccount(account.publicKey, USDC_MINT.address);
      await bridgeAccountSign(transaction, signers);
    }
    loadBalances();
    setAddUSDCMintLoading(false);
  }

  const addSolsToAccounts = (value: number) => () => {
    const event = new CustomEvent(SEND_SOLS_TO_ALL_ACCOUNTS, { detail: { value }});
    window.dispatchEvent(event);
  }

  const withdrawSolsFromAccounts = () => {
    const event = new CustomEvent(WITHDRAW_SOLS_FROM_ALL_ACCOUNTS);
    window.dispatchEvent(event);
  }

  const addUSDCToAccounts = (value: number) => () => {
    const event = new CustomEvent(SEND_USDC_TO_ALL_ACCOUNTS, { detail: { value }});
    window.dispatchEvent(event);
  }

  const withdrawUSDCFromAccounts = () => {
    const event = new CustomEvent(WITHDRAW_USDC_FROM_ALL_ACCOUNTS);
    window.dispatchEvent(event);
  }

  const addUSDCMintToAllAccount = () => {
    const event = new CustomEvent(ADD_USDC_TOKEN);
    window.dispatchEvent(event);
  }

  return (
    <>
      <Wrapper lg={12}>
        <h3 style={{ wordBreak: "break-word" }}>Wallet Account: { wallet.publicKey.toBase58() }</h3>
        <h3>Sol balance: { walletBalance.sol }</h3>
        <h3>USDC balance: { walletBalance.usdc }</h3>
        <h3>Has USDC account: { walletBalance.hasUsdc.toString() }</h3>
        <h3 style={{ wordBreak: "break-word", marginTop: 16 }}>Bridge Account: { account.publicKey.toBase58() }</h3>
        <h3>Sol balance: { accountBalance.sol }</h3>
        <h3>USDC balance: { accountBalance.usdc }</h3>
        <h3>Has USDC account: { accountBalance.hasUsdc.toString() }</h3>
        <div style={{ marginTop: 16 }}>
          <ButtonGroup>
            <Button disabled={loading} onClick={loadBalances}>Reload</Button>
          </ButtonGroup>
        </div>
        <div style={{ marginTop: 8 }}>
          <ButtonGroup>
            <Button disabled={loading} onClick={() => setTransferSolModalIsVisible(true)}>Transfer SOL</Button>
            <Button disabled={loading} onClick={() => setWithdrawSolModalIsVisible(true)}>Withdraw SOL</Button>
          </ButtonGroup>
        </div>
        <div style={{ marginTop: 8 }}>
          <ButtonGroup>
            { !accountBalance.hasUsdc && <Button onClick={addUSDCMint} loading={addUSDCMintLoading} disabled={addUSDCMintLoading || loading}>Add USDC mint</Button> }
            { accountBalance.hasUsdc && <Button disabled={loading} onClick={() => setTransferUSDCModalIsVisible(true)}>Transfer USDC</Button>}
            { accountBalance.hasUsdc && <Button disabled={loading} onClick={() => setWithdrawUSDCModalIsVisible(true)}>Withdraw USDC</Button>}
          </ButtonGroup>
        </div>
        <div style={{ marginTop: 8 }}>
          <h3>Accounts SOL commands</h3>
          <ButtonGroup>
            <Button onClick={addSolsToAccounts(0.1)}>+0.1</Button>
            <Button onClick={addSolsToAccounts(0.2)}>+0.2</Button>
            <Button onClick={addSolsToAccounts(0.3)}>+0.3</Button>
            <Button onClick={withdrawSolsFromAccounts}>Withdraw</Button>
          </ButtonGroup>
        </div>
        <div style={{ marginTop: 8 }}>
          <h3>Accounts USDC commands</h3>
          <ButtonGroup>
            <Button onClick={addUSDCMintToAllAccount}>Add USDC Tokens</Button>
            <Button onClick={addUSDCToAccounts(0.1)}>+0.1</Button>
            <Button onClick={addUSDCToAccounts(45)}>+45</Button>
            <Button onClick={addUSDCToAccounts(100)}>+100</Button>
            <Button onClick={addUSDCToAccounts(450)}>+450</Button>
            <Button onClick={withdrawUSDCFromAccounts}>Withdraw</Button>
          </ButtonGroup>
        </div>
        <div style={{ marginTop: 32 }}>
          <h3>Accounts</h3>
        </div>
        <Modal
          visible={transferSolModalIsVisible}
          onCancel={() => {
            if (!transferSolLoading && !loading) {
              setTransferSolModalIsVisible(false)
            }
          }}
          onOk={transferSol}
          okButtonProps={{
            disabled: transferSolLoading || loading,
            loading: transferSolLoading || loading
          }}
          cancelButtonProps={{
            disabled: transferSolLoading || loading,
          }}
          title="Сколько SOL перекинуть на аккаунт-мост"
        >
          <Input value={solToTransfer} onChange={(e) => setSolToTransfer(e.target.value)} type="number" />
        </Modal>
        <Modal
          visible={withdrawSolModalIsVisible}
          onCancel={() => {
            if (!withdrawSolLoading && !loading) {
              setWithdrawSolLoading(false)
            }
          }}
          onOk={withdrawSol}
          okButtonProps={{
            disabled: withdrawSolLoading || loading,
            loading: withdrawSolLoading || loading
          }}
          cancelButtonProps={{
            disabled: withdrawSolLoading || loading,
          }}
          title="Сколько SOL перекинуть на основной аккаунт"
        >
          <Input value={solToWithdraw} onChange={(e) => setSolToWithdraw(e.target.value)} type="number" />
        </Modal>
        <Modal
          visible={transferUSDCModalIsVisible}
          onCancel={() => {
            if (!transferUSDCLoading && !loading) {
              setTransferUSDCModalIsVisible(false)
            }
          }}
          onOk={transferUSDC}
          okButtonProps={{
            disabled: transferUSDCLoading || loading,
            loading: transferUSDCLoading || loading
          }}
          cancelButtonProps={{
            disabled: transferUSDCLoading || loading,
          }}
          title="Сколько USDC перекинуть на аккаунт-мост"
        >
          <Input value={usdcToTransfer} onChange={(e) => setUSDCToTransfer(e.target.value)} type="number" />
        </Modal>
        <Modal
          visible={withdrawUSDCModalIsVisible}
          onCancel={() => {
            if (!withdrawUSDCLoading && !loading) {
              setWithdrawUSDCModalIsVisible(false)
            }
          }}
          onOk={withdrawUSDC}
          okButtonProps={{
            disabled: withdrawUSDCLoading || loading,
            loading: withdrawUSDCLoading || loading
          }}
          cancelButtonProps={{
            disabled: withdrawUSDCLoading || loading,
          }}
          title="Сколько USDC перекинуть на основной аккаунт?"
        >
          <Input value={usdcToWithdraw} onChange={(e) => setUSDCToWithdraw(e.target.value)} type="number" />
        </Modal>
      </Wrapper>
      <Wrapper lg={24} md={24}>
        <AccountsTable
          accounts={accounts}
          bridgeAccountSign={bridgeAccountSign}
          bridgePublicKey={account.publicKey}
          reload={loadBalances}
        />
      </Wrapper>
    </>
  );
};

export default Accounts;