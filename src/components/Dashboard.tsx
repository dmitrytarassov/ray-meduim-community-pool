import React, {useEffect} from 'react';
import {WalletAdapter} from "../wallet-adapters";
import ConfigGenerator, {getAccountFromPrivateKey} from "./ConfigGenerator";
import {Account, PublicKey, Transaction} from "@solana/web3.js";
import Accounts from "./Accounts";

export type ConfigAccount = {
  privateKey: Buffer | Uint8Array | Array<number>,
  publicKey: PublicKey,
}

export type Config = {
  bridgeAccount: ConfigAccount,
  accounts: ConfigAccount[],
}

const Dashboard = ({ wallet, signWallet }: { wallet: WalletAdapter, signWallet: (tx: Transaction, signers?: Account[]) => void }) => {
  const [config, setConfig] = React.useState<Config>();
  const [accounts, setAccounts] = React.useState<Account[]>();
  const [bridgeAccount, setBridgeAccount] = React.useState<Account>();

  useEffect(() => {
    if (config) {
      setBridgeAccount(getAccountFromPrivateKey(config.bridgeAccount.privateKey));
      const accounts = config.accounts.map((acc) => getAccountFromPrivateKey(acc.privateKey));
      setAccounts(accounts);
    }
  }, [config]);

  return (
    <>
      { !config ? <ConfigGenerator onConfigChange={setConfig} /> : <>
        { bridgeAccount && accounts?.length && <Accounts wallet={wallet} account={bridgeAccount} signWallet={signWallet} accounts={accounts} /> }
      </> }
    </>
  );
};

export default Dashboard;