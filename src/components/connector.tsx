import React, {useState} from 'react';
import {SolletExtensionAdapter} from "../wallet-adapters/sollet-extension";
import {SolongWalletAdapter} from "../wallet-adapters/solong";
import {PhantomWalletAdapter} from "../wallet-adapters/phantom";
import {MathWalletAdapter} from "../wallet-adapters/math";
import {Button, Col, Row, Select} from "antd";
import Wrapper from "./Wrapper";

export const WALLET_PROVIDERS = [
  {
    name: 'sollet.io',
    url: 'https://www.sollet.io',
  },
  {
    name: 'Sollet Extension',
    url: 'https://www.sollet.io/extension',
    adapter: SolletExtensionAdapter as any,
  },
  {
    name: 'Solong',
    url: 'https://www.solong.com',
    adapter: SolongWalletAdapter,
  },
  {
    name: 'Phantom',
    url: 'https://www.phantom.app',
    adapter: PhantomWalletAdapter,
  },
  {
    name: 'MathWallet',
    url: 'https://www.mathwallet.org',
    adapter: MathWalletAdapter,
  }
];

const Connector = ({ onConnect }: { onConnect: (name: string) => void }) => {
  const [value, setValue] = useState<undefined | string>(undefined);

  const onClickHandler = () => {
    if (value) {
      onConnect(value);
    }
  }

  return (
    <>
      <Wrapper style={{ marginBottom: 8 }}>
        <Select style={{ width: '100%' }} placeholder="Select provider" value={value} onChange={setValue}>
          { WALLET_PROVIDERS.map(provider => (
            <Select.Option value={provider.url} key={provider.name}>{provider.name}</Select.Option>
          )) }
        </Select>
      </Wrapper>
      <Wrapper>
        <Button disabled={!value} onClick={onClickHandler}>Connect</Button>
      </Wrapper>
    </>
  );
};

export default Connector;