import React, {useEffect, useState} from 'react';
import Wrapper from "./Wrapper";
import {Button, Input} from "antd";
import {Account, PublicKey} from "@solana/web3.js";
import {Config} from "./Dashboard";
import TextArea from "antd/es/input/TextArea";

function download(filename: string, text: string) {
  const element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

const saveConfig = (config: Config) => {
  const {bridgeAccount, accounts} = config;
  const textConfig: any = {};
  textConfig.bridgeAccount = {
    publicKey: bridgeAccount.publicKey.toBase58(),
    // @ts-ignore
    privateKey: [...bridgeAccount.privateKey]
  }
  textConfig.accounts = [];
  accounts.forEach(acc => {
    textConfig.accounts.push({
      publicKey: acc.publicKey.toBase58(),
      // @ts-ignore
      privateKey: [...acc.privateKey]
    })
  });
  download("config.json", JSON.stringify(textConfig, undefined, 2));
}

const createConfig = (count: number): Config => {
  const account = new Account();
  const config: Config = {
    bridgeAccount: {
      publicKey: account.publicKey,
      privateKey: account.secretKey
    },
    accounts: []
  };
  for (let i = 0; i < count; i++) {
    const _account = new Account();
    config.accounts.push({
      publicKey: _account.publicKey,
      privateKey: _account.secretKey
    })
  }
  return config;
}

export const getAccountFromPrivateKey = (key: Buffer | Uint8Array | Array<number>): Account => new Account(key);

const ConfigGenerator = ({ onConfigChange }: { onConfigChange: (config: Config) => void }) => {
  const [count, setCount] = useState(1);
  const [loading, setLoading] = useState(false);
  const [text, setText] = useState("");
  const [config, setConfig] = useState<Config>();
  const [error, setError] = useState(false);

  const onClickHandler = async () => {
    setLoading(true);
    setTimeout(async () => {
      const config = createConfig(count);
      saveConfig(config);
      setLoading(false);
      setCount(1);
    }, 100);
  }

  useEffect(() => {
    setError(false);
    if (text) {
      try {
        const data = JSON.parse(text);
        getAccountFromPrivateKey(data.bridgeAccount.privateKey);
        const _config: Config = {
          bridgeAccount: {
            publicKey: new PublicKey(data.bridgeAccount.publicKey),
            privateKey: data.bridgeAccount.privateKey,
          },
          accounts: []
        }
        // @ts-ignore
        data.accounts.forEach(acc => {
          getAccountFromPrivateKey(data.bridgeAccount.privateKey);
          _config.accounts.push({
            publicKey: new PublicKey(acc.publicKey),
            privateKey: acc.privateKey,
          })
        });
        setConfig(_config);
      } catch(e) {
        console.log(e);
        setError(true);
      }
    }
  }, [text]);

  const start = () => {
    if (config) {
      onConfigChange(config);
    }
  }

  return (
    <Wrapper>
      <h3>Есть есть config.json файл - вставьте его содержимое в текстовое поле</h3>
      <TextArea
        value={text}
        onChange={(e) => setText(e.target.value)}
        style={{ height: 200 }}
      />
      { error && <label style={{ color: "red" }}>Ошибка чтения данных</label>}
      { config && <Button style={{ marginTop: 8 }} onClick={start}>Начать</Button> }
      <h3 style={{ marginTop: 8 }}>Если у вас нет файла - создайте его</h3>
      <label htmlFor="">Количество аккаунтов: { count }</label>
      <Input
        type="range"
        min={1}
        max={1000}
        value={count}
        onChange={(e) => setCount(+e.target.value)}
      />
      <Button disabled={loading} loading={loading} onClick={onClickHandler}>Создать</Button>
    </Wrapper>
  );
};

export default ConfigGenerator;