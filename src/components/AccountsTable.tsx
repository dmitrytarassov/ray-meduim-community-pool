import React, {useEffect, useState} from 'react';
import {Account, PublicKey, Transaction} from "@solana/web3.js";
import {
  ADD_USDC_TOKEN,
  Balance,
  getBalancesFromBC,
  getDefaultBalance,
  SEND_SOLS_TO_ALL_ACCOUNTS, SEND_USDC_TO_ALL_ACCOUNTS,
  WITHDRAW_SOLS_FROM_ALL_ACCOUNTS, WITHDRAW_USDC_FROM_ALL_ACCOUNTS
} from "./Accounts";
import setBlockHash from "../utils/setBlockHash";
import {solanaConnection} from "../utils/getConnection";
import {SEND_OPTIONS} from "../utils/options";
import {waitTransactionConfirmation} from "../utils/waitTransactionConfirmation";
import {notify} from "../utils/notifications";
import {TOKEN_MINTS} from "@project-serum/serum";
import {createTokenAccount} from "../utils/createTokenAccount";
import {createTransferSolTransaction} from "../utils/createTransferSolTransaction";
import transferToken from "../utils/transferToken";

const AccountRow = ({
  account,
  bridgeAccountSign,
  bridgePublicKey,
  reload
}: {
  account: Account,
  bridgeAccountSign: (transaction: Transaction, signers?: Account[]) => Promise<void>
  bridgePublicKey: PublicKey,
  reload: () => Promise<void>
}) => {
  const [balance, setBalance] = useState<Balance>(getDefaultBalance());
  const [loading, setLoading] = useState(false);

  const loadBalance = async () => {
    setLoading(true);
    const balance = await getBalancesFromBC(account.publicKey);
    setBalance(balance);
    setLoading(false);
  }

  const sign = async (transaction: Transaction, signers?: Account[]) => {
    await setBlockHash(transaction);
    transaction.feePayer = account.publicKey;

    if (signers && signers.length > 0) {
      transaction.partialSign(...signers);
    }

    let signature;

    try {
      transaction.partialSign(account);
      signature = await solanaConnection.sendRawTransaction(transaction.serialize(), SEND_OPTIONS);
      await waitTransactionConfirmation(signature);
      notify({ message: "Confirmed", txid: signature });
    } catch(e) {
      console.log(e);
      notify({ message: "Error while confirming", txid: signature, type: "error" });
    }
  }

  useEffect(() => {
    loadBalance();
  }, []);

  const addUsdc = async (e: any) => {
    if (e?.preventDefault) {
      e.preventDefault();
    }

    if (!balance.hasUsdc) {
      setLoading(true);
      const USDC_MINT = TOKEN_MINTS.find(({ name }) => name === "USDC");
      if (USDC_MINT) {
        const [transaction, signers] = await createTokenAccount(account.publicKey, USDC_MINT.address);
        await sign(transaction, signers);
      }
      loadBalance();
    }
  }

  const addSol = (value: number) => async () => {
    setLoading(true);
    const transaction = createTransferSolTransaction(value, bridgePublicKey, account.publicKey);
    try {
      await bridgeAccountSign(transaction);
    } catch (e) {
      // do nothing
    }
    loadBalance();
    reload();
  }

  const withdraw = async () => {
    if (balance.sol > 0.000005) {
      setLoading(true);
      const transaction = createTransferSolTransaction((balance.sol - 0.000005), account.publicKey, bridgePublicKey);
      try {
        await sign(transaction);
      } catch (e) {
        // do nothing
      }
    }
    loadBalance();
    reload();
  }

  const depositUsdc = (value: number) => async () => {
    setLoading(true);
    const transaction = await transferToken(bridgePublicKey, account.publicKey, value);
    if (transaction) {
      if (transaction) {
        await bridgeAccountSign(transaction);
      }
    }
    loadBalance();
    reload();
  }

  const withdrawUsdc = async () => {
    setLoading(true);
    const transaction = await transferToken(account.publicKey, bridgePublicKey, balance.usdc);
    try {
      if (transaction) {
        await sign(transaction);
      }
    } catch (e) {
      // do nothing
    }
    loadBalance();
    reload();
  }

  useEffect(() => {
    const addSols = (e: any) => {
      if (e?.detail?.value) {
        addSol(+e?.detail?.value)();
      }
    };

    const addUsdcs = (e: any) => {
      if (e?.detail?.value) {
        depositUsdc(+e?.detail?.value)();
      }
    };

    window.addEventListener(SEND_SOLS_TO_ALL_ACCOUNTS, addSols);
    window.addEventListener(WITHDRAW_SOLS_FROM_ALL_ACCOUNTS, withdraw);
    window.addEventListener(ADD_USDC_TOKEN, addUsdc);
    window.addEventListener(SEND_USDC_TO_ALL_ACCOUNTS, addUsdcs);
    window.addEventListener(WITHDRAW_USDC_FROM_ALL_ACCOUNTS, withdrawUsdc);

    return () => {
      window.removeEventListener(SEND_SOLS_TO_ALL_ACCOUNTS, addSols);
      window.removeEventListener(WITHDRAW_SOLS_FROM_ALL_ACCOUNTS, withdraw);
      window.removeEventListener(ADD_USDC_TOKEN, addUsdc);
      window.removeEventListener(SEND_USDC_TO_ALL_ACCOUNTS, addUsdcs);
      window.removeEventListener(WITHDRAW_USDC_FROM_ALL_ACCOUNTS, withdrawUsdc);
    }
  }, []);

  return (
    <tr>
      <td><a href={`https://solanabeach.io/address/${account.publicKey.toBase58()}`}>{ account.publicKey.toBase58() }</a></td>
      <td>{ balance.sol }SOL
        <a className="inline" href="#" onClick={addSol(0.1)}>+0.1</a>
        <a className="inline" href="#" onClick={addSol(0.2)}>+0.2</a>
        <a className="inline" href="#" onClick={addSol(0.3)}>+0.3</a>
        <a className="inline" href="#" onClick={withdraw}>Withdraw</a>
      </td>
      <td>{ balance.hasUsdc ? "+" : <a href="#" onClick={addUsdc}>Add</a> }</td>
      <td>{ balance.hasUsdc ? <>
        { balance.usdc } USDC
        <a className="inline" href="#" onClick={depositUsdc(0.1)}>+0.1</a>
        <a className="inline" href="#" onClick={depositUsdc(50)}>+50</a>
        <a className="inline" href="#" onClick={depositUsdc(100)}>+100</a>
        <a className="inline" href="#" onClick={depositUsdc(450)}>+450</a>
        <a className="inline" href="#" onClick={withdrawUsdc}>Withdraw</a>
      </> : "-" }</td>
      <td>{ loading ? "Loading..." : <>
        <a href="#" onClick={loadBalance}>Reload</a>
      </>}</td>
    </tr>
  );
}

const AccountsTable = ({ accounts, bridgeAccountSign, bridgePublicKey, reload }: {
  accounts: Account[],
  bridgeAccountSign: (transaction: Transaction, signers?: Account[]) => Promise<void>,
  bridgePublicKey: PublicKey,
  reload: () => Promise<void>
}) => {
  return (
    <table style={{ width: '100%' }}>
      <thead>
        <tr>
          <th>Account id</th>
          <th>Sol balance</th>
          <th>Has USDC</th>
          <th>USDC balance</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
      {
        accounts.map(acc => <AccountRow account={acc} key={acc.publicKey.toBase58()} bridgeAccountSign={bridgeAccountSign} bridgePublicKey={bridgePublicKey} reload={reload} />)
      }
      </tbody>
    </table>
  );
};

export default AccountsTable;