import React from 'react';
import {Col, Row} from "antd";

const Wrapper = ({ children, style = {}, md = 12, lg=6 }: { children: any, style?: any, md?: number, lg?: number }) => {
  return (
    <Row justify="center" align="middle" style={style}>
      <Col md={md} lg={lg} xs={24}>
        { children }
      </Col>
    </Row>
  );
};

export default Wrapper;