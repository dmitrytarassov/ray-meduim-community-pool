import Wallet from '@project-serum/sol-wallet-adapter';
import React, {useEffect} from 'react';
import 'antd/dist/antd.css';
import './App.css';
import Connector, {WALLET_PROVIDERS} from "./components/connector";
import {WalletAdapter} from "./wallet-adapters";
import {notify} from "./utils/notifications";
import {Col, Row} from "antd";
import Dashboard from "./components/Dashboard";
import {Account, Transaction} from "@solana/web3.js";
import setBlockHash from "./utils/setBlockHash";
import {solanaConnection} from "./utils/getConnection";
import {SEND_OPTIONS} from "./utils/options";
import {waitTransactionConfirmation} from "./utils/waitTransactionConfirmation";

function App() {
  const [connected, setConnected] = React.useState(false);
  const [wallet, setWallet] = React.useState<WalletAdapter>();

  const connect = (providerUrl: string) => {
    const provider = WALLET_PROVIDERS.find(({ url }) => url === providerUrl);
    if (provider) {
      const updateWallet = () => {
        // hack to also update wallet synchronously in case it disconnects
        // eslint-disable-next-line react-hooks/exhaustive-deps
        const wallet = new (provider.adapter || Wallet)(
          providerUrl,
          "https://api.mainnet-beta.solana.com",
        ) as WalletAdapter;
        setWallet(wallet);
      }

      if (document.readyState !== 'complete') {
        // wait to ensure that browser extensions are loaded
        const listener = () => {
          updateWallet();
          window.removeEventListener('load', listener);
        };
        window.addEventListener('load', listener);
        return () => window.removeEventListener('load', listener);
      } else {
        updateWallet();
      }
    }
  }

  useEffect(() => {
    console.log(wallet);
    if (wallet) {
      wallet.connect();
      wallet.on('connect', () => {
        if (wallet?.publicKey) {
          setConnected(true);
          const walletPublicKey = wallet.publicKey.toBase58();
          const keyToDisplay =
            walletPublicKey.length > 20
              ? `${walletPublicKey.substring(
              0,
              7,
              )}.....${walletPublicKey.substring(
              walletPublicKey.length - 7,
              walletPublicKey.length,
              )}`
              : walletPublicKey;

          notify({
            message: 'Wallet update',
            description: 'Connected to wallet ' + keyToDisplay,
          });
        }
      });

      wallet.on('disconnect', () => {
        setConnected(false);
        notify({
          message: 'Wallet update',
          description: 'Disconnected from wallet',
        });
        localStorage.removeItem('feeDiscountKey');
      });
    }
  }, [wallet]);

  const signWallet = async (transaction: Transaction, signers?: Account[]): Promise<void> => {
    if (wallet) {
      await setBlockHash(transaction);
      transaction.feePayer = wallet.publicKey;

      if (signers && signers.length > 0) {
        transaction.partialSign(...signers);
      }

      let signature;

      try {
        const tx = await wallet.signTransaction(transaction);
        signature = await solanaConnection.sendRawTransaction(tx.serialize(), SEND_OPTIONS);
        await waitTransactionConfirmation(signature);
        notify({ message: "Confirmed", txid: signature });
      } catch(e) {
        notify({ message: "Error while confirming", txid: signature, type: "error" });
      }
    }

    return;
  }

  return (
    <div className="App">
      <Row justify="center" align="middle" style={{ height: "100vh" }}>
        <Col span={24}>
          { connected && wallet ? <Dashboard wallet={wallet} signWallet={signWallet} /> : <Connector onConnect={connect} /> }
        </Col>
      </Row>
    </div>
  );
}

export default App;
