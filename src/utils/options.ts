import { SendOptions } from '@solana/web3.js';

export const SEND_OPTIONS: SendOptions = {
  skipPreflight: false,
  preflightCommitment: 'max',
};
