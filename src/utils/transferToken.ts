import {getTokenAccountInfo} from "./getTokenAccountInfo";
import {TOKEN_MINTS} from "@project-serum/serum";
import {createTokenTransferInstruction} from "./createTokenTransferInstruction";
import {Account, PublicKey, Transaction, TransactionInstruction} from "@solana/web3.js";
import {solanaConnection} from "./getConnection";
import {parseTokenMintData} from "./getAllMintInfo";

export default async function transferToken(from: PublicKey, to: PublicKey, amount: number): Promise<Transaction | null> {
  const fromTokens = await getTokenAccountInfo(from);
  const toTokens = await getTokenAccountInfo(to);
  const USDC_TOKEN = TOKEN_MINTS.find(({ name }) => name === "USDC");
  if (USDC_TOKEN) {
    const fromUSDCToken = fromTokens.find(({ effectiveMint }) => effectiveMint.equals(USDC_TOKEN.address));
    const toUSDCToken = toTokens.find(({ effectiveMint }) => effectiveMint.equals(USDC_TOKEN.address));
    if (fromUSDCToken) {
      if (toUSDCToken) {
        const mintAccount = (await solanaConnection.getAccountInfo(fromUSDCToken.effectiveMint))!;
        const mintData = parseTokenMintData(mintAccount.data);

        const instruction = createTokenTransferInstruction(
          fromUSDCToken.pubkey,
          toUSDCToken.pubkey,
          +amount * Math.pow(10, mintData.decimals),
          from
        );
        const transaction = new Transaction();
        transaction.add(instruction);
        return transaction;
      } else {
        alert("У вас нет аккаунта USDC на аккаунте, на который вы хотите послать USDC");
      }
    } else {
      alert("У вас нет аккаунта USDC на аккаунте, с которого вы хотите послать USDC");
    }
  }
  return null;
}