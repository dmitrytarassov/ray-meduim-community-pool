import {LAMPORTS_PER_SOL, PublicKey, SystemProgram, Transaction} from "@solana/web3.js";

export const createTransferSolTransaction = (amount: number, fromTokenAccount: PublicKey, toTokenAccount: PublicKey): Transaction => {
  const instruction = SystemProgram.transfer({
    fromPubkey: fromTokenAccount,
    toPubkey: toTokenAccount,
    lamports: amount * LAMPORTS_PER_SOL,
  })

  console.log(amount, amount * LAMPORTS_PER_SOL);

  const transaction = new Transaction();
  transaction.add(instruction);

  return transaction;
}