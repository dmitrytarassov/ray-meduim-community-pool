import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import {TOKEN_MINTS} from "@project-serum/serum";

export type MintToTickers = {
  [key: string]: string;
};

export default function getMintsConvertedToTickers(): MintToTickers {
  const mintsToTickers = Object.fromEntries(
    TOKEN_MINTS.map((mint) => [mint.address.toBase58(), mint.name]),
  );
  mintsToTickers[WRAPPED_SOL_MINT.toBase58()] = "SOL";
  return mintsToTickers;
}
