import { PublicKey } from '@solana/web3.js';
import getAllMintInfo, { MintInfos } from './getAllMintInfo';
import { getAllMarketsData } from './getAllMarketsData';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import { parseTokenAccountData } from './parseTokenAccountData';
import { divideBnToNumber, getTokenMultiplierFromDecimals } from './utils';
// @ts-ignore
import BN from 'bn.js';
import { getTokenAccountInfo } from './getTokenAccountInfo';
import { TokenAccount } from './types';
// import getAllOpenOrdersBalances from './getAllOpenOrdersBalances';
import getMintsConvertedToTickers from './getMintsConvertedToTickers';
import {solanaConnection} from "./getConnection";

type TokenBalances = {
  mint: string;
  balance: number;
}

async function getTokenBalances(tokenAccounts: TokenAccount[], mintInfos: MintInfos): Promise<TokenBalances[]> {
  const balances: { [mint: string]: number } = {};
  for (const account of tokenAccounts || []) {
    if (!account.account && !account.effectiveMint.equals(WRAPPED_SOL_MINT)) {
      continue;
    }
    let parsedAccount;
    if (account.effectiveMint.equals(WRAPPED_SOL_MINT)) {
      parsedAccount = {
        mint: WRAPPED_SOL_MINT,
        owner: account.pubkey,
        amount: account.account?.lamports,
      };
    } else {
      if (account.account?.data) {
        parsedAccount = parseTokenAccountData(account.account.data);
      }
    }
    if (parsedAccount) {
      if (!(parsedAccount.mint.toBase58() in balances)) {
        balances[parsedAccount.mint.toBase58()] = 0;
      }
      const mintInfo = mintInfos[parsedAccount.mint.toBase58()];
      const additionalAmount = divideBnToNumber(
        new BN(parsedAccount.amount),
        getTokenMultiplierFromDecimals(mintInfo?.decimals || 0),
      );
      balances[parsedAccount.mint.toBase58()] += additionalAmount;
    }
  }
  return Object.entries(balances).map(([mint, balance]) => {
    return {
      mint,
      balance
    };
  });
}

export type Balances = {
  coin: string;
  mint: string;
  walletBalance: number;
  openOrdersFree: number;
  openOrdersTotal: number;
}

const balancesLoading: { [key: string]: Promise<Balances[]>} = {}

export default async function getAllBalances(
  accountPublicKey: PublicKey,
  _loadOpenOrdersBalances?: boolean,
  tokenAccounts?: TokenAccount[]
): Promise<Balances[]> {
  const connection = solanaConnection;
  if (!balancesLoading[accountPublicKey.toBase58()]) {
    balancesLoading[accountPublicKey.toBase58()] = new Promise(async (resolve, reject) => {
      const allMarkets = await getAllMarketsData(connection);
      if (!tokenAccounts) {
        tokenAccounts = await getTokenAccountInfo(accountPublicKey);
      }
      const mintInfos = await getAllMintInfo(connection, accountPublicKey, allMarkets, tokenAccounts);
      const tokenBalances = await getTokenBalances(tokenAccounts, mintInfos);
      const mintToTickers = getMintsConvertedToTickers();

      const ret = await Promise.all(tokenBalances.map(async (balance) => {
        const balances: Balances = {
          coin: mintToTickers[balance.mint],
          mint: balance.mint,
          walletBalance: balance.balance,
          openOrdersFree: 0,
          openOrdersTotal: 0,
        };
        if (_loadOpenOrdersBalances) {
          // const openOrdersBalances = await getAllOpenOrdersBalances(connection, accountPublicKey, allMarkets, mintInfos);
          // for (const openOrdersAccount of openOrdersBalances[balance.mint] || []) {
          //   balances.openOrdersFree += openOrdersAccount.free;
          //   balances.openOrdersTotal += openOrdersAccount.total;
          // }
        }
        return balances;
      }));
      resolve(ret);
      delete balancesLoading[accountPublicKey.toBase58()];
    });
  }
  return balancesLoading[accountPublicKey.toBase58()];
}