import { memoize } from 'lodash';
import {Market, MARKETS, TOKEN_MINTS} from '@project-serum/serum';
import { Connection, PublicKey } from '@solana/web3.js';
import { MarketInfo } from './types';

const _MARKETS = MARKETS.filter((m) => !(m.name === "OXY/USDT" && m.address.toBase58() === "HdBhZrnrxpje39ggXnTb6WuTWVvj5YKcSHwYGQCRsVj"));

export type MarketData = {
  baseName: string;
  quoteName: string;
  market: Market;
  marketName: string;
  programId: PublicKey;
  address: PublicKey;
  deprecated: boolean;
}

async function getAllMarketsDataFunc(connection: Connection, allMarkets = true): Promise<MarketData[]> {
  const marketInfos: MarketInfo[] = allMarkets ? _MARKETS : _MARKETS.filter(({ deprecated }) => !deprecated);

  const markets: (MarketData | null)[] = await Promise.all(
    marketInfos.map(async (marketInfo) => {
      try {
        const market = await Market.load(
          connection,
          marketInfo.address,
          {},
          marketInfo.programId,
        );

        // @ts-ignore
        const base = TOKEN_MINTS.find(({ address }) => address.equals(market.baseMintAddress));
        // @ts-ignore
        const quote = TOKEN_MINTS.find(({ address }) => address.equals(market.quoteMintAddress));
        const [baseName = "", quoteName = ""] = marketInfo.name.split("/");

        return {
          baseName: base ? base.name : baseName,
          quoteName: quote ? quote.name : quoteName,
          deprecated: Boolean(marketInfo.deprecated),
          market,
          marketName: marketInfo.name,
          programId: marketInfo.programId,
          address: marketInfo.address,
        };
      } catch (e) {
        throw new Error("Error loading all markets");
      }
    }),
  );

  return markets.filter((m): m is MarketData => !!m);
}

export const getAllMarketsData = memoize(getAllMarketsDataFunc, (a, b) => b);
