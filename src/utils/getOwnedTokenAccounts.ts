// @ts-ignore
import bs58 from 'bs58';
import { AccountInfo, Connection, PublicKey } from '@solana/web3.js';
// @ts-ignore
import * as BufferLayout from 'buffer-layout';
import {solanaConnection} from "./getConnection";

export const TOKEN_PROGRAM_ID = new PublicKey(
  'TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA',
);

export type TokenAccount = {
  publicKey: PublicKey;
  accountInfo: AccountInfo<Buffer>
};

type Requests = {
  [key: string]: Promise<TokenAccount[]>;
}
const requests: Requests = {};

export const ACCOUNT_LAYOUT = BufferLayout.struct([
  BufferLayout.blob(32, 'mint'),
  BufferLayout.blob(32, 'owner'),
  BufferLayout.nu64('amount'),
  BufferLayout.blob(93),
]);


export function getOwnedAccountsFilters(publicKey: PublicKey) {
  return [
    {
      memcmp: {
        offset: ACCOUNT_LAYOUT.offsetOf('owner'),
        bytes: publicKey.toBase58(),
      },
    },
    {
      dataSize: ACCOUNT_LAYOUT.span,
    },
  ];
}

export async function getOwnedTokenAccounts(
  accountPublicKey: PublicKey,
): Promise<TokenAccount[]> {
  const connection = solanaConnection;
  const accountPublicKeyString = accountPublicKey.toBase58();
  if (!requests[accountPublicKeyString]) {
    requests[accountPublicKeyString] = new Promise(async resolve => {
      const filters = getOwnedAccountsFilters(accountPublicKey);
      // @ts-ignore
      const resp = await connection._rpcRequest('getProgramAccounts', [
        TOKEN_PROGRAM_ID.toBase58(),
        {
          commitment: connection.commitment,
          filters,
        },
      ]);
      if (resp.error) {
        throw new Error(`failed to get token accounts owned by ${accountPublicKey.toBase58()}`)
      }
      const result = resp.result
        // @ts-ignore
        .map(({ pubkey, account: { data, executable, owner, lamports } }) => ({
          publicKey: new PublicKey(pubkey),
          accountInfo: {
            data: bs58.decode(data),
            executable,
            owner: new PublicKey(owner),
            lamports,
          },
        }))
        // @ts-ignore
        .filter(({ accountInfo }) => {
          // @ts-ignore
          return filters.every((filter) => {
            if (filter.dataSize) {
              return accountInfo.data.length === filter.dataSize;
            } else if (filter.memcmp) {
              const filterBytes = bs58.decode(filter.memcmp.bytes);
              return accountInfo.data
                .slice(
                  filter.memcmp.offset,
                  filter.memcmp.offset + filterBytes.length,
                )
                .equals(filterBytes);
            }
            return false;
          });
        });

      resolve(result);
      delete requests[accountPublicKeyString];
    });
  }

  return requests[accountPublicKeyString];
}
