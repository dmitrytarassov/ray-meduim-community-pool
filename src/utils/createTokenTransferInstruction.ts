import {PublicKey} from "@solana/web3.js";
import {TOKEN_PROGRAM_ID, Token} from '@solana/spl-token';

export function createTokenTransferInstruction(
  source: PublicKey,
  destination: PublicKey,
  amount: number,
  accountPublicKey: PublicKey,
) {
  return Token.createTransferInstruction(
    TOKEN_PROGRAM_ID,
    source,
    destination,
    accountPublicKey,
    [],
    amount,
  );
}