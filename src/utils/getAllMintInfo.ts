import { Connection, PublicKey } from '@solana/web3.js';
import { getTokenAccountInfo } from './getTokenAccountInfo';
import { getMultipleSolanaAccounts } from './getMultipleSolanaAccounts';
import { MarketData } from './getAllMarketsData';
import { TokenAccount } from './types';
// @ts-ignore
import BN from "bn.js";
// @ts-ignore
import * as BufferLayout from 'buffer-layout';

export const MINT_LAYOUT = BufferLayout.struct([
  BufferLayout.u32('authority'),
  BufferLayout.blob(32),
  BufferLayout.blob(8, 'supply'),
  BufferLayout.u8('decimals'),
  BufferLayout.u8('initialized'),
  BufferLayout.blob(36),
]);

export interface MintInfo {
  decimals: number;
  initialized: boolean;
  supply: BN;
  mintAddress?: PublicKey;
}

// @ts-ignore
export function parseTokenMintData(data): MintInfo {
  const { decimals, initialized, supply } = MINT_LAYOUT.decode(data);
  return {
    decimals,
    initialized: !!initialized,
    supply: new BN(supply, 10, 'le'),
  };
}

export type MintInfos = {
  [key: string]: MintInfo
}

export default async function getAllMintInfo(
  connection: Connection,
  accountPublicKey: PublicKey,
  allMarkets: MarketData[],
  tokenAccounts?: TokenAccount[]
): Promise<MintInfos> {
  if (!tokenAccounts) {
    tokenAccounts = await getTokenAccountInfo(accountPublicKey);
  }

  const allMints = (tokenAccounts || [])
    .map((account) => account.effectiveMint)
    .concat(
      (allMarkets || []).map((marketInfo) => marketInfo.market.baseMintAddress),
    )
    .concat(
      (allMarkets || []).map(
        (marketInfo) => marketInfo.market.quoteMintAddress,
      ),
    );
  // @ts-ignore
  const uniqueMints = [...new Set(allMints.map((mint) => mint.toBase58()))].map(
    (stringMint) => new PublicKey(stringMint),
  );

  const mintInfos = await getMultipleSolanaAccounts(connection, uniqueMints);

  return Object.fromEntries(
    Object.entries(mintInfos.value).map(([key, accountInfo]) => [
      key,
      // @ts-ignore
      accountInfo && parseTokenMintData(accountInfo.data),
    ]).filter(([, accountInfo]) => accountInfo),
  );
}