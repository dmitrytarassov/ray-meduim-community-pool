import { Transaction } from '@solana/web3.js';
import {solanaConnection} from "./getConnection";

export default async function setBlockHash(tx: Transaction): Promise<void> {
  const connection = solanaConnection;
  const { blockhash } = await connection.getRecentBlockhash('max');
  tx.recentBlockhash = blockhash;
}
