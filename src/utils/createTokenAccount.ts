import { Account, Connection, PublicKey, SystemProgram, Transaction } from '@solana/web3.js';
import { TokenInstructions } from '@project-serum/serum';
import {solanaConnection} from "./getConnection";

export async function createTokenAccount(
  accountPublicKey: PublicKey,
  mintPublicKey: PublicKey
): Promise<[Transaction, Account[]]> {
  const connection = solanaConnection;
  const newAccount = new Account();
  const transaction = new Transaction();
  const instruction = SystemProgram.createAccount({
    fromPubkey: accountPublicKey,
    newAccountPubkey: newAccount.publicKey,
    lamports: await connection.getMinimumBalanceForRentExemption(165),
    space: 165,
    programId: TokenInstructions.TOKEN_PROGRAM_ID,
  });
  transaction.add(instruction);

  transaction.add(
    TokenInstructions.initializeAccount({
      account: newAccount.publicKey,
      mint: mintPublicKey,
      owner: accountPublicKey,
    }),
  );

  return [transaction, [newAccount]];
}