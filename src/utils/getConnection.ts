import {Connection} from "@solana/web3.js";

export const solanaConnection = new Connection("https://api.mainnet-beta.solana.com");