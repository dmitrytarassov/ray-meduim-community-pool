import { useCallback, useEffect, useMemo, useState } from 'react';
import { PublicKey } from '@solana/web3.js';
// @ts-ignore
import BN from 'bn.js';

export function isValidPublicKey(key: string) {
  if (!key) {
    return false;
  }
  try {
    return !!new PublicKey(key);
  } catch {
    return false;
  }
}
export const percentFormat = new Intl.NumberFormat(undefined, {
  style: 'percent',
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export function floorToDecimal(value: number, decimals: number | undefined | null) {
  return decimals ? Math.floor(value * 10 ** decimals) / 10 ** decimals : value;
}

export function roundToDecimal(value: number, decimals: number | undefined | null) {
  return decimals ? Math.round(value * 10 ** decimals) / 10 ** decimals : value;
}

export function createRoundToDecimal(decimals: number) {
  const mul = 10 ** decimals;
  return function (value: number) {
    return Math.round(value * mul) / mul;
  };
}

export function getDecimalCount(value: any): number {
  if (!isNaN(value) && Math.floor(value) !== value && value.toString().includes('.'))
    return value.toString().split('.')[1].length || 0;
  if (!isNaN(value) && Math.floor(value) !== value && value.toString().includes('e'))
    return parseInt(value.toString().split('e-')[1] || '0', 10);
  return 0;
}

export function divideBnToNumber(numerator: BN, denominator: BN): number {
  const quotient = numerator.div(denominator).toNumber();
  const rem = numerator.umod(denominator);
  const gcd = rem.gcd(denominator);
  return quotient + rem.div(gcd).toNumber() / denominator.div(gcd).toNumber();
}

export function getTokenMultiplierFromDecimals(decimals: number): BN {
  return new BN(10).pow(new BN(decimals));
}

function getValue<T>(source: (() => T) | T | null): T | null {
  if (typeof source === 'function') {
    return (source as () => T)();
  }
  return source;
}
