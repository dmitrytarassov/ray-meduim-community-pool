import { PublicKey } from '@solana/web3.js';
import { TokenAccount } from './types';
import { WRAPPED_SOL_MINT } from '@project-serum/serum/lib/token-instructions';
import { getOwnedTokenAccounts } from './getOwnedTokenAccounts';
import { parseTokenAccountData } from './parseTokenAccountData';
import {solanaConnection} from "./getConnection";

type Requests = {
  [key: string]: Promise<TokenAccount[]>;
}
const requests: Requests = {};

export async function getTokenAccountInfo(
  accountPublicKey: PublicKey,
): Promise<TokenAccount[]> {
  const connection = solanaConnection;
  const accountPublicKeyString = accountPublicKey.toBase58();
  if (!requests[accountPublicKeyString]) {
    requests[accountPublicKeyString] = new Promise(async resolve => {
      const [splAccounts, account] = await Promise.all([
        getOwnedTokenAccounts(accountPublicKey),
        connection.getAccountInfo(accountPublicKey),
      ]);

      const parsedSplAccounts: TokenAccount[] = splAccounts.map(
        ({ publicKey, accountInfo }) => {
          return {
            pubkey: publicKey,
            account: accountInfo,
            effectiveMint: parseTokenAccountData(accountInfo.data).mint,
          };
        },
      );

      resolve(parsedSplAccounts.concat({
        pubkey: accountPublicKey,
        account,
        effectiveMint: WRAPPED_SOL_MINT,
      }));

      delete requests[accountPublicKeyString];
    });
  }

  return requests[accountPublicKeyString]
}
